def call(String gitProject) {

    pipeline {

        agent any

        stages {

            stage('Clone') {

                steps {

                    git url: "${gitProject}"

                }

            }

            stage('Compile') {

                steps {

                    sh "./mvnw package"

                }

            }

        }

    }

}
